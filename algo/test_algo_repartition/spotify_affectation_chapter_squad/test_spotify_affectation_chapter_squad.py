from spotify_affectation_chapter_squad import (
    condition_not_squad_empty,
    distribute_people,
    check_chapter_completeness,
)

groups_squad = ["squad_1", "squad_2", "squad_3"]
people3 = ["p1", "p2", "p3"]
people4 = ["p1", "p2", "p3", "p4"]


def test_condition_not_squad_empty_ok():
    aaa = {
        "squad": {
            "p1": "squad_2",
            "p2": "squad_1",
            "p3": "squad_3",
            "p4": "squad_2",
            "p5": "squad_1",
            "p6": "squad_2",
            "p7": "squad_2",
            "p8": "squad_2",
            "p9": "squad_1",
            "p10": "squad_1",
        },
        "chapter": {
            "p1": "chapter_1",
            "p2": "chapter_2",
            "p3": "chapter_2",
            "p4": "chapter_2",
            "p5": "chapter_2",
            "p6": "chapter_2",
            "p7": "chapter_2",
            "p8": "chapter_2",
            "p9": "chapter_2",
            "p10": "chapter_2",
        },
    }
    assert condition_not_squad_empty(aaa, groups_squad) == True


def test_condition_not_squad_empty_KO():
    aaa = {
        "squad": {
            "p1": "squad_2",
            "p2": "squad_2",
            "p3": "squad_2",
            "p4": "squad_2",
            "p5": "squad_1",
            "p6": "squad_2",
            "p7": "squad_2",
            "p8": "squad_2",
            "p9": "squad_1",
            "p10": "squad_1",
        },
        "chapter": {
            "p1": "chapter_1",
            "p2": "chapter_2",
            "p3": "chapter_2",
            "p4": "chapter_2",
            "p5": "chapter_2",
            "p6": "chapter_2",
            "p7": "chapter_2",
            "p8": "chapter_2",
            "p9": "chapter_2",
            "p10": "chapter_2",
        },
    }
    assert condition_not_squad_empty(aaa, groups_squad) == False


import json


def util_compare_lists(list1, list2):
    try:
        sorted_list1 = sorted([json.dumps(d, sort_keys=True) for d in list1])
        sorted_list2 = sorted([json.dumps(d, sort_keys=True) for d in list2])
    except TypeError:
        return False
    # breakpoint()()
    return sorted_list1 == sorted_list2


def test_distribute_1_people_2_squad():
    res = distribute_people(["p1"], ["squad_1", "squad_2"])
    assert res == []


def test_distribute_2_people_1_squad():
    res = distribute_people(["p1", "p2"], ["squad_1"])
    assert util_compare_lists(res, [{"p1": "squad_1", "p2": "squad_1"}])


def test_distribute_1_people_1_squad():
    res = distribute_people(["p1"], ["squad_1"])
    assert util_compare_lists(res, [{"p1": "squad_1"}])


def test_distribute_2_people_2_squad():
    res = distribute_people(["p1", "p2"], ["squad_1", "squad_2"])
    assert util_compare_lists(
        res,
        [
            {"p1": "squad_1", "p2": "squad_1"},
            {"p1": "squad_1", "p2": "squad_2"},
            {"p1": "squad_2", "p2": "squad_1"},
            {"p1": "squad_2", "p2": "squad_2"},
        ],
    )


def test_distribute_2_people_3_squad():
    res = distribute_people(["p1", "p2"], ["squad_1", "squad_2", "squad_3"])
    assert res == []


def test_distribute_3_people_2_squad():
    res = distribute_people(["p1", "p2", "p3"], ["squad_1", "squad_2"])
    # breakpoint()()
    assert util_compare_lists(
        res,
        [
            {"p1": "squad_1", "p2": "squad_1", "p3": "squad_1"},
            {"p1": "squad_1", "p2": "squad_1", "p3": "squad_2"},
            {"p1": "squad_1", "p2": "squad_2", "p3": "squad_1"},
            {"p1": "squad_1", "p2": "squad_2", "p3": "squad_2"},
            {"p1": "squad_2", "p2": "squad_1", "p3": "squad_1"},
            {"p1": "squad_2", "p2": "squad_1", "p3": "squad_2"},
            {"p1": "squad_2", "p2": "squad_2", "p3": "squad_1"},
            {"p1": "squad_2", "p2": "squad_2", "p3": "squad_2"},
        ],
    )


def test_distribute_3_people_3_squad():
    res = distribute_people(["p1", "p2", "p3"], ["squad_1", "squad_2", "squad_3"])
    assert util_compare_lists(
        res,
        [
            {"p1": "squad_1", "p2": "squad_1", "p3": "squad_1"},
            {"p1": "squad_1", "p2": "squad_1", "p3": "squad_2"},
            {"p1": "squad_1", "p2": "squad_2", "p3": "squad_1"},
            {"p1": "squad_1", "p2": "squad_2", "p3": "squad_2"},
            {"p1": "squad_1", "p2": "squad_2", "p3": "squad_3"},
            {"p1": "squad_1", "p2": "squad_3", "p3": "squad_1"},
            {"p1": "squad_1", "p2": "squad_3", "p3": "squad_2"},
            {"p1": "squad_1", "p2": "squad_3", "p3": "squad_3"},
            {"p1": "squad_1", "p2": "squad_1", "p3": "squad_3"},
            {"p1": "squad_2", "p2": "squad_1", "p3": "squad_1"},
            {"p1": "squad_2", "p2": "squad_1", "p3": "squad_2"},
            {"p1": "squad_2", "p2": "squad_2", "p3": "squad_1"},
            {"p1": "squad_2", "p2": "squad_2", "p3": "squad_2"},
            {"p1": "squad_2", "p2": "squad_2", "p3": "squad_3"},
            {"p1": "squad_2", "p2": "squad_3", "p3": "squad_1"},
            {"p1": "squad_2", "p2": "squad_3", "p3": "squad_2"},
            {"p1": "squad_2", "p2": "squad_3", "p3": "squad_3"},
            {"p1": "squad_2", "p2": "squad_1", "p3": "squad_3"},
            {"p1": "squad_3", "p2": "squad_1", "p3": "squad_1"},
            {"p1": "squad_3", "p2": "squad_1", "p3": "squad_2"},
            {"p1": "squad_3", "p2": "squad_2", "p3": "squad_1"},
            {"p1": "squad_3", "p2": "squad_2", "p3": "squad_2"},
            {"p1": "squad_3", "p2": "squad_2", "p3": "squad_3"},
            {"p1": "squad_3", "p2": "squad_3", "p3": "squad_1"},
            {"p1": "squad_3", "p2": "squad_3", "p3": "squad_2"},
            {"p1": "squad_3", "p2": "squad_3", "p3": "squad_3"},
            {"p1": "squad_3", "p2": "squad_1", "p3": "squad_3"},
        ],
    )


def test_distribute_4_people_3_squad():
    res = distribute_people(["p1", "p2", "p3", "p4"], ["squad_1", "squad_2", "squad_3"])
    assert util_compare_lists(
        res,
        [
            {"p1": "squad_1", "p2": "squad_1", "p3": "squad_1", "p4": "squad_1"},
            {"p1": "squad_1", "p2": "squad_1", "p3": "squad_1", "p4": "squad_2"},
            {"p1": "squad_1", "p2": "squad_1", "p3": "squad_1", "p4": "squad_3"},
            {"p1": "squad_1", "p2": "squad_1", "p3": "squad_2", "p4": "squad_1"},
            {"p1": "squad_1", "p2": "squad_1", "p3": "squad_2", "p4": "squad_2"},
            {"p1": "squad_1", "p2": "squad_1", "p3": "squad_2", "p4": "squad_3"},
            {"p1": "squad_1", "p2": "squad_1", "p3": "squad_3", "p4": "squad_1"},
            {"p1": "squad_1", "p2": "squad_1", "p3": "squad_3", "p4": "squad_2"},
            {"p1": "squad_1", "p2": "squad_1", "p3": "squad_3", "p4": "squad_3"},
            {"p1": "squad_1", "p2": "squad_2", "p3": "squad_1", "p4": "squad_1"},
            {"p1": "squad_1", "p2": "squad_2", "p3": "squad_1", "p4": "squad_2"},
            {"p1": "squad_1", "p2": "squad_2", "p3": "squad_1", "p4": "squad_3"},
            {"p1": "squad_1", "p2": "squad_2", "p3": "squad_2", "p4": "squad_1"},
            {"p1": "squad_1", "p2": "squad_2", "p3": "squad_2", "p4": "squad_2"},
            {"p1": "squad_1", "p2": "squad_2", "p3": "squad_2", "p4": "squad_3"},
            {"p1": "squad_1", "p2": "squad_2", "p3": "squad_3", "p4": "squad_1"},
            {"p1": "squad_1", "p2": "squad_2", "p3": "squad_3", "p4": "squad_2"},
            {"p1": "squad_1", "p2": "squad_2", "p3": "squad_3", "p4": "squad_3"},
            {"p1": "squad_1", "p2": "squad_3", "p3": "squad_1", "p4": "squad_1"},
            {"p1": "squad_1", "p2": "squad_3", "p3": "squad_1", "p4": "squad_2"},
            {"p1": "squad_1", "p2": "squad_3", "p3": "squad_1", "p4": "squad_3"},
            {"p1": "squad_1", "p2": "squad_3", "p3": "squad_2", "p4": "squad_1"},
            {"p1": "squad_1", "p2": "squad_3", "p3": "squad_2", "p4": "squad_2"},
            {"p1": "squad_1", "p2": "squad_3", "p3": "squad_2", "p4": "squad_3"},
            {"p1": "squad_1", "p2": "squad_3", "p3": "squad_3", "p4": "squad_1"},
            {"p1": "squad_1", "p2": "squad_3", "p3": "squad_3", "p4": "squad_2"},
            {"p1": "squad_1", "p2": "squad_3", "p3": "squad_3", "p4": "squad_3"},
            {"p1": "squad_2", "p2": "squad_1", "p3": "squad_1", "p4": "squad_1"},
            {"p1": "squad_2", "p2": "squad_1", "p3": "squad_1", "p4": "squad_2"},
            {"p1": "squad_2", "p2": "squad_1", "p3": "squad_1", "p4": "squad_3"},
            {"p1": "squad_2", "p2": "squad_1", "p3": "squad_2", "p4": "squad_1"},
            {"p1": "squad_2", "p2": "squad_1", "p3": "squad_2", "p4": "squad_2"},
            {"p1": "squad_2", "p2": "squad_1", "p3": "squad_2", "p4": "squad_3"},
            {"p1": "squad_2", "p2": "squad_1", "p3": "squad_3", "p4": "squad_1"},
            {"p1": "squad_2", "p2": "squad_1", "p3": "squad_3", "p4": "squad_2"},
            {"p1": "squad_2", "p2": "squad_1", "p3": "squad_3", "p4": "squad_3"},
            {"p1": "squad_2", "p2": "squad_2", "p3": "squad_1", "p4": "squad_1"},
            {"p1": "squad_2", "p2": "squad_2", "p3": "squad_1", "p4": "squad_2"},
            {"p1": "squad_2", "p2": "squad_2", "p3": "squad_1", "p4": "squad_3"},
            {"p1": "squad_2", "p2": "squad_2", "p3": "squad_2", "p4": "squad_1"},
            {"p1": "squad_2", "p2": "squad_2", "p3": "squad_2", "p4": "squad_2"},
            {"p1": "squad_2", "p2": "squad_2", "p3": "squad_2", "p4": "squad_3"},
            {"p1": "squad_2", "p2": "squad_2", "p3": "squad_3", "p4": "squad_1"},
            {"p1": "squad_2", "p2": "squad_2", "p3": "squad_3", "p4": "squad_2"},
            {"p1": "squad_2", "p2": "squad_2", "p3": "squad_3", "p4": "squad_3"},
            {"p1": "squad_2", "p2": "squad_3", "p3": "squad_1", "p4": "squad_1"},
            {"p1": "squad_2", "p2": "squad_3", "p3": "squad_1", "p4": "squad_2"},
            {"p1": "squad_2", "p2": "squad_3", "p3": "squad_1", "p4": "squad_3"},
            {"p1": "squad_2", "p2": "squad_3", "p3": "squad_2", "p4": "squad_1"},
            {"p1": "squad_2", "p2": "squad_3", "p3": "squad_2", "p4": "squad_2"},
            {"p1": "squad_2", "p2": "squad_3", "p3": "squad_2", "p4": "squad_3"},
            {"p1": "squad_2", "p2": "squad_3", "p3": "squad_3", "p4": "squad_1"},
            {"p1": "squad_2", "p2": "squad_3", "p3": "squad_3", "p4": "squad_2"},
            {"p1": "squad_2", "p2": "squad_3", "p3": "squad_3", "p4": "squad_3"},
            {"p1": "squad_3", "p2": "squad_1", "p3": "squad_1", "p4": "squad_1"},
            {"p1": "squad_3", "p2": "squad_1", "p3": "squad_1", "p4": "squad_2"},
            {"p1": "squad_3", "p2": "squad_1", "p3": "squad_1", "p4": "squad_3"},
            {"p1": "squad_3", "p2": "squad_1", "p3": "squad_2", "p4": "squad_1"},
            {"p1": "squad_3", "p2": "squad_1", "p3": "squad_2", "p4": "squad_2"},
            {"p1": "squad_3", "p2": "squad_1", "p3": "squad_2", "p4": "squad_3"},
            {"p1": "squad_3", "p2": "squad_1", "p3": "squad_3", "p4": "squad_1"},
            {"p1": "squad_3", "p2": "squad_1", "p3": "squad_3", "p4": "squad_2"},
            {"p1": "squad_3", "p2": "squad_1", "p3": "squad_3", "p4": "squad_3"},
            {"p1": "squad_3", "p2": "squad_2", "p3": "squad_1", "p4": "squad_1"},
            {"p1": "squad_3", "p2": "squad_2", "p3": "squad_1", "p4": "squad_2"},
            {"p1": "squad_3", "p2": "squad_2", "p3": "squad_1", "p4": "squad_3"},
            {"p1": "squad_3", "p2": "squad_2", "p3": "squad_2", "p4": "squad_1"},
            {"p1": "squad_3", "p2": "squad_2", "p3": "squad_2", "p4": "squad_2"},
            {"p1": "squad_3", "p2": "squad_2", "p3": "squad_2", "p4": "squad_3"},
            {"p1": "squad_3", "p2": "squad_2", "p3": "squad_3", "p4": "squad_1"},
            {"p1": "squad_3", "p2": "squad_2", "p3": "squad_3", "p4": "squad_2"},
            {"p1": "squad_3", "p2": "squad_2", "p3": "squad_3", "p4": "squad_3"},
            {"p1": "squad_3", "p2": "squad_3", "p3": "squad_1", "p4": "squad_1"},
            {"p1": "squad_3", "p2": "squad_3", "p3": "squad_1", "p4": "squad_2"},
            {"p1": "squad_3", "p2": "squad_3", "p3": "squad_1", "p4": "squad_3"},
            {"p1": "squad_3", "p2": "squad_3", "p3": "squad_2", "p4": "squad_1"},
            {"p1": "squad_3", "p2": "squad_3", "p3": "squad_2", "p4": "squad_2"},
            {"p1": "squad_3", "p2": "squad_3", "p3": "squad_2", "p4": "squad_3"},
            {"p1": "squad_3", "p2": "squad_3", "p3": "squad_3", "p4": "squad_1"},
            {"p1": "squad_3", "p2": "squad_3", "p3": "squad_3", "p4": "squad_2"},
            {"p1": "squad_3", "p2": "squad_3", "p3": "squad_3", "p4": "squad_3"},
        ],
    )
    assert len(res) == 81

def test_check_chapter_completeness():
    # Test 1: Valid chapter completeness
    one_cross_combinations = {
        "squad": {
            "p1": "squad_1",
            "p2": "squad_2",
            "p3": "squad_3",
            "p4": "squad_1",
            "p5": "squad_2",
            "p6": "squad_3",
            "p7": "squad_1",
            "p8": "squad_2",
            "p9": "squad_3",
        },
        "chapter": {
            "p1": "chapter_1",
            "p2": "chapter_1",
            "p3": "chapter_1",
            "p4": "chapter_2",
            "p5": "chapter_2",
            "p6": "chapter_2",
            "p7": "chapter_3",
            "p8": "chapter_3",
            "p9": "chapter_3",
        },
    }
    assert check_chapter_completeness(one_cross_combinations) == True


def test_check_chapter_completeness_chapter3_dont_have_squad2_member():
    one_cross_combinations = {
        "squad": {
            "p1": "squad_1",
            "p2": "squad_2",
            "p3": "squad_3",
            "p4": "squad_1",
            "p5": "squad_2",
            "p6": "squad_3",
            "p7": "squad_1",
            "p8": "squad_3",
            "p9": "squad_3",
        },
        "chapter": {
            "p1": "chapter_1",
            "p2": "chapter_1",
            "p3": "chapter_1",
            "p4": "chapter_2",
            "p5": "chapter_2",
            "p6": "chapter_2",
            "p7": "chapter_3",
            "p8": "chapter_3",
            "p9": "chapter_3",
        },
    }
    assert check_chapter_completeness(one_cross_combinations) == False


def test_check_chapter_completeness_chapter3_have_2_squad2_member():
    one_cross_combinations = {
        "squad": {
            "p1": "squad_1",
            "p2": "squad_2",
            "p3": "squad_3",
            "p4": "squad_1",
            "p5": "squad_2",
            "p6": "squad_3",
            "p7": "squad_1",
            "p8": "squad_2",
            "p9": "squad_3",
            "p10": "squad_2",
        },
        "chapter": {
            "p1": "chapter_1",
            "p2": "chapter_1",
            "p3": "chapter_1",
            "p4": "chapter_2",
            "p5": "chapter_2",
            "p6": "chapter_2",
            "p7": "chapter_3",
            "p8": "chapter_3",
            "p9": "chapter_3",
            "p10": "chapter_3",
        },
    }
    assert check_chapter_completeness(one_cross_combinations) == True