import itertools
import numpy
import pandas as pd

NBR_DEV = 8
groups_squad = ["squad_1", "squad_2", "squad_3"]
people = [f"p{i}" for i in range(1, NBR_DEV + 1)]
groups_chapter = ["chapter_1", "chapter_2", "chapter_2"]

NBR_SQUAD = len(groups_squad)
NBR_CHAPTER = len(groups_chapter)

from itertools import product


def distribute_people(people, squads):
    num_people = len(people)
    num_squads = len(squads)

    if num_people < num_squads:
        return []

    if num_squads == 1:
        res = {}
        for p in people:
            res[p] = squads[0]
        return [res]

    result = []
    for squad_combo in product(squads, repeat=len(people)):
        squad_assignment = dict(zip(people, squad_combo))
        result.append(squad_assignment)
    return result


def get_cross_combinations(combinations_squad, combinations_chapter):
    res = []
    for chapter in combinations_chapter:
        for squad in combinations_squad:
            res.append({"squad": squad, "chapter": chapter})
    return res


def condition_not_squad_empty(
    specific_cross_combinations,
    groups_squad=groups_squad,
):
    for squad in groups_squad:
        if squad not in specific_cross_combinations["squad"].values():
            return False
    return True


def condition_not_chapter_empty(
    specific_cross_combinations,
    groups_chapter=groups_chapter,
):
    for chapter in groups_chapter:
        if chapter not in specific_cross_combinations["chapter"].values():
            return False
    return True


def condition_p_is_in_squad(
    one_cross_combinations,
):
    preaffectation = {
        "p2": "squad_1",
        #       "p1": "squad_2",
        #        "p5": "squad_2",
    }
    tmp = {}
    for k_cond, v_cond in preaffectation.items():
        for k, v in one_cross_combinations["squad"].items():
            if k == k_cond:
                if v != v_cond:
                    return False
    return True


def condition_p_is_in_chapter(
    one_cross_combinations,
):
    preaffectation = {
        "p1": "chapter_1",
        "p2": "chapter_2",
    }
    tmp = {}
    for k_cond, v_cond in preaffectation.items():
        for k, v in one_cross_combinations["chapter"].items():
            if k == k_cond:
                if v != v_cond:
                    return False
    return True


def check_chapter_completeness(one_cross_combinations):
    squads = set(one_cross_combinations["squad"].values())
    chapters = set(one_cross_combinations["chapter"].values())

    for chapter in chapters:
        chapter_squads = set()
        for person, squad in one_cross_combinations["squad"].items():
            if one_cross_combinations["chapter"][person] == chapter:
                chapter_squads.add(squad)
        if squads != chapter_squads:
            return False
    return True


def apply_all_filter(specific_cross_combinations):
    for fct in [
        condition_not_squad_empty,
        condition_not_chapter_empty,
        condition_p_is_in_squad,
        condition_p_is_in_chapter,
        check_chapter_completeness,
    ]:
        if fct(specific_cross_combinations) == False:
            return False
    #create_table(specific_cross_combinations)
    return True


def filter_rules(combinations_squad, combinations_chapter):
    res = []
    cross_combinations = get_cross_combinations(
        combinations_squad, combinations_chapter
    )
    print(f"avant filtre {len(cross_combinations)}")
    for specific_cross_combinations in cross_combinations:
        if apply_all_filter(specific_cross_combinations) == True:
            res.append(specific_cross_combinations)
    print(f"apres filtre {len(res)}")
    return (len(res), res)


def create_table(dictionary):
    df = pd.DataFrame.from_dict(dictionary)

    grouped = (
        df.groupby(["chapter", "squad"])
        .apply(lambda x: ", ".join(x.index))
        .unstack(fill_value="")
    )

    # Affichage du tableau
    print(grouped)


if __name__ == "__main__":
    combinations_squad = distribute_people(people, groups_squad)
    # Print all combinations
    print(
        f"combinations_squad: Il y'a {len(combinations_squad)} et la premiere est {combinations_squad[0]}"
    )
    combinations_chapter = distribute_people(people, groups_chapter)

    # Print all combinations
    print(
        f"combinations_chapter : Il y'a {len(combinations_chapter)} et la premiere est {combinations_chapter[0]}"
    )
    # print(combinations_chapter)
    nbr, res = filter_rules(combinations_squad, combinations_chapter)
    print(nbr)
    r = len(res)
    if r != 0:
        if r < 10:
            print(res[0])
            create_table(res[0])
        else : 
            print(f"il y'a {r} resultats")
    else:
        print(f"il ny'a aucun resultat")
