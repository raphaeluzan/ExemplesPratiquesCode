from sklearn.decomposition import PCA
from sklearn import datasets

iris = datasets.load_iris()
X = iris.data

pca = PCA(n_components=3)

df = pca.fit_transform(X)

print(f"La PCA a ete effectué sur {pca.n_components_} composants")
print(f"Les composants permettant de faie la transformation sont : {pca.components_}")
print(df)
