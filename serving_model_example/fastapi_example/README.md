# How to run it ?

0. tester avec Python 
```
3.9.5
```

1. installer les dependances
```
pip install fastapi
```

2. Lancement de l'app streamlit
```
uvicorn main:app
```

3. aller sur la page :
```
http://localhost:8000
```
