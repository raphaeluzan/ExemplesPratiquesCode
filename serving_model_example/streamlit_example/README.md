# How to run it ?

0. tester avec Python 
```
3.9.5
```

1. installer les dependances
```
pip install -r  requirements.txt 
```

2. Lancement de l'app streamlit
```
streamlit run streamlit.py
```

3. aller sur la page :
```
http://localhost:8501
```